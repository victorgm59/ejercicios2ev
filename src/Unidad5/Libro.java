package Unidad5;

import java.util.ArrayList;

public class Libro {
	
	private String titulo;
	private ArrayList<Autor> autores;
	private float precio;
	private int stock;
	
	public Libro(String titulo, ArrayList<Autor> autores, float precio) {
		this(titulo, autores, precio, 0);
	}
	public Libro(String titulo, ArrayList<Autor> autores, float precio, int stock) {
		this.titulo = titulo;
		this.autores = autores;
		this.precio = precio;		
		this.stock = stock;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	@Override
	public String toString() {
		return "titulo_libro " + titulo + "(" + autores + ")" + "precio � " + precio + " - stock unidades stock " + stock;
	}
	
}
