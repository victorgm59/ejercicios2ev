package Unidad5;

import java.time.LocalDate;
public class Animal {
		
		private String nombre;
		private LocalDate fecha;
		
		public Animal(String nombre, LocalDate fecha) {
			this.nombre = nombre;
			this.fecha = fecha;
		}
		public Animal(String nombre) {
			this.nombre = nombre;
			fecha = LocalDate.now();
		}
		public void setNombre(String animal) {
			this.nombre = animal;
		}
		public void setFecha(LocalDate fecha) {
			this.fecha = fecha;
		}
		public String getNombre() {
			return nombre;
		}
		public LocalDate getFecha() {
			return fecha;
		}
		
		@Override
		public String toString() {
			return "Animal nombre= " + nombre + ", fecha= " + fecha;
		}
	


}
