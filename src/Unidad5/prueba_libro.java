package Unidad5;

import java.util.ArrayList;

public class prueba_libro {

	public static void main(String[] args) {
		ArrayList<Autor> autores = new ArrayList<>();
		autores.add(new Autor("Mar�a", "maria@gmail.com", Genero.femenino));
		autores.add(new Autor("Fernando", "fernando@gmail.com", Genero.masculino));
		Libro libro1 = new Libro ("Perico", autores, 50, 30);
		System.out.println(libro1);
	}

}
